﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Restaurant.Domain;


namespace Restaurant.API
{
	public class DatabaseContext : DbContext
	{
		public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
		{
		}
		public DbSet<Manager> Managers { get; set; }
		public DbSet<RestaurantBranch> Restaurants { get; set; }
		public DbSet<OrderTable> OrderTables { get; set; }
		public DbSet<Waiter> Waiters { get; set; }
	}
}
