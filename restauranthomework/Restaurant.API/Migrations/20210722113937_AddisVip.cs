﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Restaurant.API.Migrations
{
    public partial class AddisVip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isVip",
                table: "OrderTables",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isVip",
                table: "OrderTables");
        }
    }
}
