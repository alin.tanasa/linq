﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
namespace Restaurant.Domain
{
	public class Manager : BaseEntity
	{
		[Required]
		[MinLength(2)]
		[MaxLength(100)]
		public string Name { get; set; }

		public string Email { get; set; }

		public virtual RestaurantBranch Restaurant { get; set; }

	}
}
