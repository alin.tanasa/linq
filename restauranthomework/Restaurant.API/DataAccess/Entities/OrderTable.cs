﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Restaurant.Domain
{
	public class OrderTable : BaseEntity
	{
		[Required]public int Number { get; set; }
		public bool isVip { get; set; }

		public OrderTable(int number, bool isvip)
		{
			Number = number;
			isVip = isvip;
		}

		public OrderTable()
		{
			Number = 0;
			isVip = true;
		}
	}
}
